# The Lobster Pot Games

This program will play the lobster pot game with my stratergy without me needing to sit and do the maths.

An example rule set can be found at [http://www.mrbartonmaths.com/blog/tes-maths-rotw-21-lobster-game/](http://www.mrbartonmaths.com/blog/tes-maths-rotw-21-lobster-game/)

## Settings

These are the rules that you can adjust (a default value is also provided)

| Setting                                         | Default Value            | Notes                                                                              |
| ----------------------------------------------- | ------------------------ | ---------------------------------------------------------------------------------- |
| Starting money                                  | £80                      |                                                                                    |
| Starting lobster pots                           | 5                        |                                                                                    |
| Starting boats                                  | 1                        |                                                                                    |
| Rent amount                                     | 50\* number of boats +30 |                                                                                    |
| Rent frequency                                  | 7 days                   |                                                                                    |
| Cost of placing a pot onshore                   | £0                       | This setting will be greater than 0 if boats are used                              |
| Cost of placing a pot offshore                  | £0                       | This setting will be greater than 0 if boats are used                              |
| Boat cost                                       | £150                     | If boats are not being used set to 0                                               |
| Boat capacity                                   | 6                        | If boats are not being used set to > 0                                             |
| Huricanes                                       | False                    |                                                                                    |
| Huricanes frequency                             | 3                        | After how many days of bad weather will a huricane occur                           |
| Role that give good weather                     | 1, 2, 3, 4               | Normally a D6 if used                                                              |
| Roles that give bad weather                     | 6                        |                                                                                    |
| Roles that give the same weather as previous go | 5                        |                                                                                    |
| Good weather onshore return                     | 3                        |                                                                                    |
| Good weather offshore return                    | 5                        |                                                                                    |
| Bad weather onshore return                      | 5                        |                                                                                    |
| Selling a boat                                  | 75                       | If you would otherwise go into debt you can sell your boat for half what you payed |
| Debt interest                                   | 10%                      |                                                                                    |
| Cost of a pot                                   | £20                      |                                                                                    |

# Explanation of stratergy

I am quite risk averse hence I want to play this game such that I get roughly the same amount of money if the dice comes up with bad weather or good.

We use an "ideal ratio" that we aim for, using deault settings this is 5/7 and place our pots according to these settings