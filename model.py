#!/usr/bin/env python3

import math
import random


# Program
def play():
    days = 600
    # Settings
    rent = -80
    rentFreq = 7
    hurican = False
    huricanFreq = 3
    interest = "money * 1.1"
    potCost = 20
    onshorePotPlacementCost = 0
    offshorePotPlacementCost = 0
    boatCost = 150
    boatCapacity = 6
    goodRoles = [1, 2, 3, 4]
    badRoles = [6]
    mimicRoles = [5]
    sellBoat = 75
    goodOnshoreReturn = 3
    goodOffshoreReturn = 5
    badOnshoreReturn = 5
    badOffshoreReturn = 0

    # Variables
    money = 80
    pots = 5
    boats = 1
    weather = 0  # 1 = good, 2 = bad
    prevWeather = 1  # 1 = good, 2 = bad
    day = 1
    keepGoing = 0
    # code for 1 turn (this will go in a while loop)
    while days != day:
        costs = 0
        profit = money
        # Determine the probability of good weather this turn
        if prevWeather == 1:
            pGood = len(goodRoles) + len(mimicRoles)
            total = len(goodRoles) + len(mimicRoles) + len(badRoles)
            pGood = pGood / total
        else:
            pGood = len(goodRoles)
            total = len(goodRoles) + len(mimicRoles) + len(badRoles)
            pGood = pGood / total
        pBad = 1-pGood

        if money < 0:
            money *= interest
        # Working out our ideal ratio of offshore pots to onshore

        # Working out our ideal ratio of offshore pots to onshore
        # Strat 3--all onshore
        #idealRatio = 0
        # Strat 2--factoring in weather
        #idealRatio = ((pGood*goodOffshoreReturn)+(pBad*badOffshoreReturn)) / (((pGood*goodOffshoreReturn)+(pBad*badOffshoreReturn))+((pGood*goodOnshoreReturn)+(pBad*badOnshoreReturn)))
        # Strat 1--Cover all bases
        idealRatio = (goodOffshoreReturn-badOffshoreReturn) / ((goodOffshoreReturn-badOffshoreReturn) + (badOnshoreReturn-goodOnshoreReturn))
        
        # Begining of turn log to user
        if prevWeather == 1:
            hprevWeather = "good"
        else:
            hprevWeather = "bad"


        boatsNeeded = math.floor(((((money/potCost)+pots)*idealRatio)-(boats*boatCapacity))/boatCapacity)
        if boatsNeeded < 0:
            boatsNeeded = 0
        if boatsNeeded == 0:
            potsNeeded = math.floor(money/potCost)
        elif money < boatsNeeded * boatCost:
            boatsNeeded = 0
            potsNeeded = 0
        elif money < -1:
            boatsNeeded = math.ceil(money/sellBoat)

        money = money - (potsNeeded*potCost)
        pots += potsNeeded

        boats += boatsNeeded
        money = money - boatsNeeded * boatCost

        costs = (boatsNeeded * boatCost)+(potsNeeded*potCost)
        # Log of what we bought

        # Where are we placing each pot
        offshore = math.floor(pots*(idealRatio))
        if offshore > boats * boatCapacity:  # checking we have the boat capacity to do what we want
            offshore = boats * boatCapacity
        onshore = pots - offshore
        # Log of where we are placing out pots

        # Determine how we did given the weather
        weather = random.randint(1, 6)
        if weather in goodRoles:
            weather = 1
            hweather = "good"
        elif weather in badRoles:
            weather = 2
            hweather = "bad"
        elif weather in mimicRoles:
            weather = prevWeather
            hweather = hprevWeather
        if weather == 1:
            onshore = goodOnshoreReturn*onshore
            offshore = goodOffshoreReturn*offshore
        else:
            pots = pots - offshore
            onshore = badOnshoreReturn * onshore
            offshore = badOffshoreReturn * offshore

        money = money+onshore+offshore
        profit = money-profit
        if day % 7 == 0:
            costs += rent
            money -= money
        # Output how we did to the user so they can record it in the table

        day += 1
        prevWeather = weather
        print(f"{day}, {money}, {pots}, {boats}, {money+(20*pots)+(150*boats)}")


print("Day, Money, Pots, Boats, Value")
play()