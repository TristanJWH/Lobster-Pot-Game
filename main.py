#!/usr/bin/env python3

import math
import random


# Program
def play(settings):
    # Settings
    rent = settings["rent"]
    rentFreq = settings["rentFreq"]
    interest = settings["interest"]
    potCost = settings["potCost"]
    boatCost = settings["boatCost"]
    boatCapacity = settings["boatCapacity"]
    goodRoles = settings["goodRoles"]
    badRoles = settings["badRoles"]
    mimicRoles = settings["mimicRoles"]
    sellBoat = settings["sellBoat"]
    goodOnshoreReturn = settings["goodOnshoreReturn"]
    goodOffshoreReturn = settings["goodOffshoreReturn"]
    badOnshoreReturn = settings["badOnshoreReturn"]
    badOffshoreReturn = settings["badOffshoreReturn"]
    money = settings["money"]
    pots = settings["pots"]
    boats = settings["boats"]
    weather = settings["weather"]  # 1 = good, 2 = bad
    prevWeather = settings["prevWeather"]  # 1 = good, 2 = bad
    day = settings["day"]
    keepGoing = settings["keepGoing"]

    # code for 1 turn (this will go in a while loop)
    while keepGoing != "-1":
        costs = 0
        profit = money
        # Rent
        if day % rentFreq == 0:
            costs += rent
            money += rent
        # Determine the probability of good weather this turn
        if prevWeather == 1:
            pGood = len(goodRoles) + len(mimicRoles)
            total = len(goodRoles) + len(mimicRoles) + len(badRoles)
            pGood = pGood / total
        else:
            pGood = len(goodRoles)
            total = len(goodRoles) + len(mimicRoles) + len(badRoles)
            pGood = pGood / total
        pBad = 1-pGood

        if money < 0:
            money *= interest

        # Working out our ideal ratio of offshore pots to onshore
        # Strat 3--all onshore
        # idealRatio = 0
        # Strat 2--factoring in weather
        #idealRatio = ((pGood*goodOffshoreReturn)+(pBad*badOffshoreReturn)) / (((pGood*goodOffshoreReturn)+(pBad*badOffshoreReturn))+((pGood*goodOnshoreReturn)+(pBad*badOnshoreReturn)))
        # Strat 1--Cover all bases
        idealRatio = (goodOffshoreReturn-badOffshoreReturn) / ((goodOffshoreReturn -
                                                                badOffshoreReturn) + (badOnshoreReturn-goodOnshoreReturn))

        # Begining of turn log to user
        if prevWeather == 1:
            hprevWeather = "good"
        else:
            hprevWeather = "bad"
        print("Start D/M/P/B")
        print(day, "/", money, "/", pots, "/", boats)

        boatsNeeded = math.floor(
            ((((money/potCost)+pots)*idealRatio)-(boats*boatCapacity))/boatCapacity)
        if boatsNeeded < 0:
            boatsNeeded = 0
        if boatsNeeded == 0:
            potsNeeded = math.floor(money/potCost)
        elif money < boatsNeeded * boatCost:
            boatsNeeded = 0
            potsNeeded = 0
        elif money < -1:
            boatsNeeded = math.ceil(money/sellBoat)

        money = money - (potsNeeded*potCost)
        pots += potsNeeded

        boats += boatsNeeded
        money = money - boatsNeeded * boatCost

        costs = (boatsNeeded * boatCost)+(potsNeeded*potCost)
        # Log of what we bought
        print("Bought M/P/B")
        print(money, "/", potsNeeded, "/", boatsNeeded)

        # Where are we placing each pot
        offshore = math.floor(pots*(idealRatio))
        if offshore > boats * boatCapacity:  # checking we have the boat capacity to do what we want
            offshore = boats * boatCapacity
        onshore = pots - offshore
        # Log of where we are placing out pots
        print("Onshore pots: ", onshore)
        print("Offshore pots: ", offshore)

        # Determine how we did given the weather
        weather = int(input("Dice role: "))
        if weather in goodRoles:
            weather = 1
            hweather = "good"
        elif weather in badRoles:
            weather = 2
            hweather = "bad"
        elif weather in mimicRoles:
            weather = prevWeather
            hweather = hprevWeather
        if weather == 1:
            onshore = goodOnshoreReturn*onshore
            offshore = goodOffshoreReturn*offshore
        else:
            pots = pots - offshore
            onshore = badOnshoreReturn * onshore
            offshore = badOffshoreReturn * offshore

        money = money+onshore+offshore
        profit = money-profit

        # Output how we did to the user so they can record it in the table
        print("Onshore: £", onshore)
        print("Offshore: £", offshore)
        print("P/C/M")
        print(profit, "/", costs, "/", money)
        day += 1
        prevWeather = weather
        keepGoing = input(
            "Continue:")
        print("\n\n\n\n\n")


gameSettings = {
    "rent": -80,
    "rentFreq": 7,
    "interest": "money * 1.1",
    "potCost": 20,
    "boatCost": 150,
    "boatCapacity": 6,
    "goodRoles": [1, 2, 3, 4],
    "badRoles": [6],
    "mimicRoles": [5],
    "sellBoat": 75,
    "goodOnshoreReturn": 3,
    "goodOffshoreReturn": 5,
    "badOnshoreReturn": 5,
    "badOffshoreReturn": 0,
    "money": 80,
    "pots": 5,
    "boats": 1,
    "weather": 0,
    "prevWeather": 1,
    "day": 1,
    "keepGoing": 0
}

def set():
    settings = {
        "rent": -80,
        "rentFreq": 7,
        "interest": "money * 1.1",
        "potCost": 20,
        "boatCost": 150,
        "boatCapacity": 6,
        "goodRoles": [1, 2, 3, 4],
        "badRoles": [6],
        "mimicRoles": [5],
        "sellBoat": 75,
        "goodOnshoreReturn": 3,
        "goodOffshoreReturn": 5,
        "badOnshoreReturn": 5,
        "badOffshoreReturn": 0,
        "money": 80,
        "pots": 5,
        "boats": 1,
        "weather": 0,
        "prevWeather": 1,
        "day": 1,
        "keepGoing": 0
    }
    settings["rent"] = int(input("Rent: "))
    settings["rentFreq"] = int(input("Rent Frequency: "))
    settings["interest"] = "money * ",input('interest: ')
    settings["potCost"] = int(input("Cost of a pot: "))
    settings["boatCost"] = int(input("Cost of a boat: "))
    settings["boatCapacity"] = int(input("Boat capacity: "))
    settings["goodRoles"] = list(input("Good roles: "))
    settings["badRoles"] = list(input("Bad roles: "))
    settings["mimicRoles"] = list(input("Mimic roles: "))
    settings["sellBoat"] = int(input("Sell a boat: "))
    settings["goodOnshoreReturn"] = int(input("goodOnshoreReturn: "))
    settings["goodOffshoreReturn"] = int(input("goodOffshoreReturn: "))
    settings["badOnshoreReturn"] = int(input("badOnshoreReturn: "))
    settings["badOffshoreReturn"] = int(input("badOffshoreReturn: "))
    settings["money"] = int(input("Starting money: "))
    settings["pots"] = int(input("Starting pots: "))
    settings["boats"] = int(input("Starting boats: "))
    return settings


play(set())